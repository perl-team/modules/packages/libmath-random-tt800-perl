Source: libmath-random-tt800-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               perl-xs-dev,
               perl:native
Standards-Version: 3.9.5
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libmath-random-tt800-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libmath-random-tt800-perl.git
Homepage: https://metacpan.org/release/Math-Random-TT800

Package: libmath-random-tt800-perl
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends}
Description: Perl module implementing the TT800 algorithm
 Math::Random::TT800 provides an implementation of Makoto Matsumoto and Takuji
 Nishimura's pseudorandom number generator (PRNG) algorithm called TT800. It is
 similar to some other fantastic PRNG algorithms such as the Mersenne Twister
 (see libmath-random-mt-perl) and ISAAC (see libmath-random-isaac-perl). This
 particular implementation is particularly notable because it has no external
 dependencies aside from Perl itself.
 .
 The algorithm itself is described in Matsumoto's article published in ACM
 Transactions on Modelling and Computer Simulation, Volume 4, Issue 3, 1994,
 pages 254-266.
 .
 This algorithm is similar to the Mersenne Twister algorithm but uses a smaller
 array to hold state information (25 elements compared to MT's 624).
 Consequently, the period is much smaller - 2^800-1 versus MT's 2^19937-1.
 The period of ISAAC is 2^8295 values on average.
