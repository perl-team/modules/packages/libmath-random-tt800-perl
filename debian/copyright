Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Math-Random-TT800
Upstream-Contact: Otmar Lendl <lendl@cosy.sbg.ac.at>
Source: https://metacpan.org/release/Math-Random-TT800

Files: *
Copyright: 1997, Otmar Lendl <lendl@cosy.sbg.ac.at>
License: Artistic or GPL-1+

Files: tt800_core.c tt800.h
Copyright: 1996, Makoto Matsumoto <m-mat@math.sci.hiroshima-u.ac.jp>
License: BSD-3
Comment: There is no license text in the included files, however, the
 M. Matsumoto has a copy of the code released under the new BSD License.
 See: http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/VERSIONS/C-LANG/tt800.c

Files: debian/*
Copyright: 2009, Jonathan Yu <jawnsy@cpan.org>
License: Artistic or GPL-1+

License: BSD-3
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The names of its contributors may not be used to endorse or promote
     products derived from this software without specific prior written
     permission.

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
